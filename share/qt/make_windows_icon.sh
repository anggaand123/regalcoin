#!/bin/bash
# create multiresolution windows icon
ICON_DST=../../src/qt/res/icons/REGALCOIN.ico

convert ../../src/qt/res/icons/REGALCOIN-16.png ../../src/qt/res/icons/REGALCOIN-32.png ../../src/qt/res/icons/REGALCOIN-48.png ${ICON_DST}
